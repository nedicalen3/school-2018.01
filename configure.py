import argparse
import json
import os

parser = argparse.ArgumentParser(description="Configure program")
subparsers = parser.add_subparsers(help="Choosing which side should we configure", dest="command")

parser_node = subparsers.add_parser("node", help="Configure node")
parser_node.add_argument("master_uri", help="Uri of master")
parser_node.add_argument("id", help="ID of this node")
parser_node.add_argument("port", help="Port of node")
parser_node.add_argument("swap", help="Size of swap file (in G)", default="5G")
parser_node.add_argument("config", help="Path to config.json", default="node/config.json")

parser_master = subparsers.add_parser("master", help="Configure master")
parser_master.add_argument("id", help="ID of master")
parser_master.add_argument("port", help="Port of master")
parser_master.add_argument("config", help="Path to config.json", default="master/config.json")

args = parser.parse_args()
if args.command == "node":
    with open(args.config, "r+") as config_file:
        config = json.load(config_file)
        config_file.seek(0)
        config_file.truncate()
        config["master_uri"] = args.master_uri
        config["node_id"] = args.id
        config["port"] = args.port
        json.dump(config, config_file)
    swap_file_command = "size={} && file_swap=/swapfile_$size.img " \
                        "&& sudo touch $file_swap && sudo fallocate" \
                        " -l $size /$file_swap && sudo mkswap " \
                        "/$file_swap && sudo swapon -p 20 /$file_swap".format(args.swap)
    os.system(swap_file_command)

else:
    with open(args.config, "r+") as config_file:
        config = json.load(config_file)
        config_file.seek(0)
        config_file.truncate()
        config["port"] = args.port
        json.dump(config, config_file)
