import sys

if (sys.version_info[0] != 3):
    raise OSError("Wrong python version!Expected 3.x.x found %d.%d.%d" % (
        sys.version_info[0], sys.version_info[1], sys.version_info[2]))

from collections import namedtuple
from http.client import responses
import json
import requests
import time


class GitException(Exception):
    pass


class GitAuthException(Exception):
    pass


class GitRepoObject:
    def __init__(self, stars, name, html_url, clone_url, size):
        self.star_count = stars
        self.name = name
        self.html_url = html_url
        self.clone_url = clone_url
        self.size = size

    def __str__(self):
        return "<%s, %s, %d, size=%d>" % (self.clone_url, self.name, self.star_count, self.size)

    def repo2tuple(self):
        return self.clone_url, self.star_count

    clone_url = ""
    star_count = -1
    html_url = "http://github.com/"
    name = "<UNNAMED>"
    size = -1


class Git:
    def __init__(self, login=None, password=None):
        '''
           __init__ -> None
           Initiallizes Git
           login:str - GitHub login
           password:str - GitHub password
           Raises:
               GitAuthException - in case of authentication failure
               GitExcpetion - in case of unknown errors in api communications
        '''
        try:
            if login != None:
                r = requests.get("http://api.github.com/rate_limit", auth=(login, password))
            else:
                r = requests.get("http://api.github.com/rate_limit")
        except Exception as e:
            raise GitException("Unable to connect:" + str(e))
        self.data = json.loads(r.text)
        if "resources" not in self.data:
            raise GitAuthException("Failed to login:" + self.data["message"])
        self._login = login
        self._password = password

    @staticmethod
    def _gitassert(cond, msg=""):
        '''
        :param:cond: Assertation condition
        :param msg: Message if assertation failes
        :return: None
        '''
        if not cond:
            raise GitException("Assertation failure:" + msg)

    @staticmethod
    def json2object(data):
        return json.loads(data, object_hook=lambda d: namedtuple('GitTemporateObject', d.keys())(*d.values()))

    def _authenticated_request(self, method, **kwargs):
        '''
        Makes request to api with auto authentication
        :param method: Git api method
        :param kwargs: passed to requests.get
        :return: dict
        '''
        s = "?" * (len(kwargs) != 0)
        for key in kwargs:
            s += "&" + key + "=" + str(kwargs[key])
        if self._login != None:
            r = requests.get("http://api.github.com/" + method + s, auth=(self._login, self._password))
        else:
            r = requests.get("http://api.github.com/" + method + s)

        self._gitassert((r.status_code == 200), "Request failed(%d %s)" % (r.status_code, responses[r.status_code]))
        return r.headers, r.text

    def search_repository(self, query, sortby="stars", orderby="desc", **kwargs):
        '''
        search_repository -> list
        Searches repos on git,returns dict of repository informations
        query:str - Query string
        sortby:str - sort criteria,default:"stars"
        orderby:str - ordering mode "asc","desc"
        **kwargs:args - GET request args(e.g. page)
        (see https://developer.github.com/v3/search/)
        '''
        headers, response = self._authenticated_request("search/repositories", q=query, sort=sortby, order=orderby,
                                                        **kwargs)
        search_remaining = int(headers["X-RateLimit-Remaining"])
        if search_remaining == 0:
            sleep_time = float(headers["X-RateLimit-Reset"]) - time.time()
            if sleep_time <= 0:
                return None
            time.sleep(sleep_time)
        items = self.json2object(response).items
        repos = []
        for repo in items:
            repos.append(GitRepoObject(html_url=repo.html_url, name=repo.full_name, stars=repo.stargazers_count,
                                       clone_url=repo.clone_url, size=repo.size))
        return repos

    def download_repository(self, repo_url):
        pass
